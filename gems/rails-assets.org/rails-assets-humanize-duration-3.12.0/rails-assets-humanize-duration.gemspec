# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-humanize-duration/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-humanize-duration"
  spec.version       = RailsAssetsHumanizeDuration::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "Convert millisecond durations to English and many other languages."
  spec.summary       = "Convert millisecond durations to English and many other languages."
  spec.homepage      = "https://github.com/EvanHahn/HumanizeDuration.js"
  spec.license       = "Unlicense"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
