# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-foundation-emails/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-foundation-emails"
  spec.version       = RailsAssetsFoundationEmails::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "A framework for designing responsive emails by ZURB."
  spec.summary       = "A framework for designing responsive emails by ZURB."
  spec.homepage      = "http://foundation.zurb.com/emails"
  spec.license       = "MIT"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]


    spec.post_install_message = "This component doesn't define main assets in bower.json.\nPlease open new pull request in component's repository:\nhttp://foundation.zurb.com/emails"

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
