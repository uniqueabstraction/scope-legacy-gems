# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-angular-leaflet-directive/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-angular-leaflet-directive"
  spec.version       = RailsAssetsAngularLeafletDirective::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "angular-leaflet-directive - An AngularJS directive to easily interact with Leaflet maps"
  spec.summary       = "angular-leaflet-directive - An AngularJS directive to easily interact with Leaflet maps"
  spec.homepage      = "http://tombatossals.github.io/angular-leaflet-directive"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_dependency "rails-assets-angular", "~> 1.0"
  spec.add_dependency "rails-assets-leaflet", "~> 0.7.0"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
