# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-d3/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-d3"
  spec.version       = RailsAssetsD3::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "A JavaScript visualization library for HTML and SVG."
  spec.summary       = "A JavaScript visualization library for HTML and SVG."
  spec.homepage      = "https://github.com/mbostock-bower/d3-bower"
  spec.license       = "BSD-3-Clause"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
