# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-ekko-lightbox/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-ekko-lightbox"
  spec.version       = RailsAssetsEkkoLightbox::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "A lightbox gallery plugin for Bootstrap 3 based on the modal plugin"
  spec.summary       = "A lightbox gallery plugin for Bootstrap 3 based on the modal plugin"
  spec.homepage      = "https://github.com/ashleydw/lightbox"
  spec.license       = "MIT"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_dependency "rails-assets-bootstrap", ">= 3.0.0"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
